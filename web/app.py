from flask import Flask
from flask import render_template
import logging
import os

app = Flask(__name__)

@app.route("/")
def hello():

    s = ["~", "//", ".."]

    #check to see if file exists
    if os.path.isfile("./templates" + flask.session):
        flask.session = open("./templates" + flask.session, "r")
        return "HTTP/1.0 200/OK\n\n"

#check for 403 error
    elif ((s[0] in flask.session) or (s[1] in flask.session) or (s[2] in flask.session):
        return error_403(403)

#404 error
    else:
        return error_404(404)

    
@app.errorhandler(404)
def error_404(404):
    app.logger.debug("File not found!") 
    return render_template('404.html'), 404
    

@app.errorhander(403)
def error_403(403):
    app.logger.debug("File is forbidden!")
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
